const sum = (a, b) => a + b
const curriedSum = a => b => a + b

module.exports = {
  sum, curriedSum
}
