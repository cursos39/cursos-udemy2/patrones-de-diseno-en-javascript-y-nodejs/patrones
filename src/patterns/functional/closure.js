const y = 'lele'

const f = () => {
  const x = 'lala'
  return () => {
    const z = 'lolo'
    return { x, y, z }
  }
}

const closureWithParam = name => () => `Damm it, ${name}`

module.exports = {
  closure: f(),
  closureWithParam
}
