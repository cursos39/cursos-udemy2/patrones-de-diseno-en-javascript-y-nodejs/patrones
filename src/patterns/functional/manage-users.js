const { compose, personToString, formatPerson, head, filterArray, isBaby, trace } = require('../../utils')

const users = [
  { age: 17, name: 'Nicolas', surName: 'Soto' },
  { age: 18, name: 'Chanchito', surName: 'Feliz' },
  { age: 12, name: 'Loreto', surName: 'Fernandez' },
  { age: 1, name: 'Sofia', surName: 'Zapata' }
]

const bringFirstBaby = compose(
  personToString,
  trace('After formatPerson '),
  formatPerson,
  head,
  filterArray(isBaby)
)

module.exports = {
  users, bringFirstBaby
}
