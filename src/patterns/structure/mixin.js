
const mixin = {
  sayHello () {
    console.log(`Hello ${this.name}`)
  },
  sayGoodBye () {
    console.log(`Chao ${this.name}`)
  }
}

class User {
  constructor (name) {
    this.name = name
  }
}

Object.assign(User.prototype, mixin)

module.exports = { User }
