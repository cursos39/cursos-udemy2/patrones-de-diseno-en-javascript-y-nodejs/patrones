class Api {
  constructor () {
    this.operations = function (url, opts, verb) {
      switch (verb) {
        case 'get':
          return 'api1-get'
        case 'post':
          return 'api1-post'
        default:
          return 0
      }
    }
  }
}

class Api2 {
  constructor () {
    this.get = function (url, opts) {
      return 'api2-get'
    }
    this.post = function (url, opts) {
      return 'api2-post'
    }
  }
}

class ApiAdapter {
  constructor () {
    const api2 = new Api2()

    this.operations = function (url, opts, verb) {
      switch (verb) {
        case 'get':
          return api2.get(url, opts)
        case 'post':
          return api2.post(url, opts)
        default:
          return 0
      }
    }
  }
}

module.exports = { Api, Api2, ApiAdapter }
