const https = require('https')

const getHostNameAndPAth = url => {
  const allUri = url.split('://')
  allUri.shift()
  const restUri = allUri[0].split('/')
  const hostname = restUri.shift()
  const path = `/${restUri.join('/')}`
  return { hostname, path }
}

const getOptions = url => ({
  ...getHostNameAndPAth(url),
  method: 'GET'
})

const get = (url, cb) => {
  const options = getOptions(url)

  const req = https.request(options, res => {
    res.setEncoding('utf8')
    let body = ''
    res.on('data', data => (body += data))

    const logResultAndReturnToCallback = end => {
      cb(undefined, JSON.parse(body))
    }

    res.on('end', logResultAndReturnToCallback)
  })

  req.on('error', error => {
    console.error(error)
    cb(error)
  })

  req.end()
}

const getPromisify = url => {
  return new Promise((resolve, reject) => {
    get(url, (error, data) => {
      if (error) {
        reject(error)
      }
      resolve(data)
    })
  })
}

module.exports = { get, getPromisify }
