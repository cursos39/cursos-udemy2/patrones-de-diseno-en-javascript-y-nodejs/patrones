class Macbook {
  constructor () {
    this.price = 1000
    this.screen = 11.6
  }
}

class HP {
  price () {
    return 500
  }
}

const increaseMemory = computer => {
  const previousPrice = computer.price()
  computer.price = function () {
    return previousPrice + 100
  }
}

const mackbook = new Macbook()

mackbook.increaseMemory = function () {
  this.price += 100
}

module.exports = {
  HP, Macbook, increaseMemory, mackbook
}
