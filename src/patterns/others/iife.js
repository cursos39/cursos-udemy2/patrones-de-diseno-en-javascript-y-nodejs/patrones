const finalSum = (() => {
  const x = 10
  const y = 2
  return x + y
})()

module.exports = {
  getIIFE: () => {
    return finalSum
  }
}
