module.exports = {
  modifyPrototypes: () => {
    // eslint-disable-next-line no-extend-native
    Object.prototype.log = function () {
      console.log(this)
    }

    if (!String.prototype.myTrim) {
      // eslint-disable-next-line no-extend-native
      String.prototype.myTrim = function () {
        try {
          return this.replace(/^\s+|\s+/g, '')
        } catch (e) {
          return this
        }
      }
    }
  },
  restart: () => {
    delete Object.prototype.log
    delete String.prototype.myTrim
  }
}
