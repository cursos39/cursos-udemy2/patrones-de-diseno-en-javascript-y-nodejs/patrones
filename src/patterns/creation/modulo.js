const modulo = {
  prop: 'mi prop',
  config: {
    language: 'es',
    cache: true
  },
  setConfig: conf => {
    modulo.config = conf
  },
  isCacheEnabled: () => {
    return modulo.config.cache ? 'yes' : 'no'
  }
}

const moduleRevelated = (() => {
  const x = {}

  return {
    returnValue: () => x,
    setProperty: (key, value) => (x[key] = value)
  }
})()

module.exports = { modulo, moduleRevelated }
