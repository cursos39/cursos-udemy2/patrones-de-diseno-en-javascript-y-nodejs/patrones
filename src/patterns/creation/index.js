class MyClass {
  constructor () {
    this.propiedad = 1
    this.metodo = () => {
      // soy un metodo
      return this.propiedad
    }
  }
}

class MyClassWithProto {
  constructor (p1, p2) {
    this.property1 = p1
    this.property2 = p2
  }

  metodo () {
    // soy un metodo
    return this.property1
  }
}

function MyFunctionClass () {
  this.prop = 1
  this.met = () => {
    return this.prop
  }
}

module.exports = { MyClass, MyFunctionClass, MyClassWithProto }
