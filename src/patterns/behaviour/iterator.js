module.exports = {
  iterator: function * iterator (collection) {
    let nextIndex = 0

    while (nextIndex < collection.length) {
      yield collection[nextIndex++]
    }
  }
}
