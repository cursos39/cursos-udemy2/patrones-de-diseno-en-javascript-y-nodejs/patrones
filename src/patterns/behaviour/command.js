const commander = (() => {
  const commands = {
    buy: ({ kind }) => `Buying a ${kind}`,
    sell: ({ kind }) => `Selling a ${kind}`
  }

  return {
    run: (commandName, commandArguments) => {
      if (!(commandName in commands)) {
        return `Command ${commandName} not found`
      }
      return commands[commandName](commandArguments)
    }
  }
})()

module.exports = { commander }
