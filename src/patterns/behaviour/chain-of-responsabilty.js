class Suma {
  constructor (v = 0) {
    this.val = v
  }

  suma (v) {
    this.val += v
    return this
  }
}

module.exports = { Suma }
