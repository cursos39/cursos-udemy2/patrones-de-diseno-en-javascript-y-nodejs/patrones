const createUser = () => {
  const events = {}

  return {
    name: 'Alberto',
    on: (event, fn) => {
      if (events[event]) {
        return events[event].push(fn)
      }
      events[event] = [fn]
    },
    trigger: (event) => {
      events[event].forEach(fn => {
        fn()
      })
    }
  }
}

const init = (user) => {
  const userLoggedIn = () => {
    user.login = true
  }
  const retrieveInitData = () => {
    user.data = ['data']
  }
  user.on('login', userLoggedIn)
  user.on('login', retrieveInitData)
}
const app = { init }

const login = (user) => {
  user.trigger('login')
}

module.exports = {
  login, app, createUser
}
