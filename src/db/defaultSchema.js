const { Schema, model } = require('mongoose')

const auditProps = {
  createdAt: { type: Date, default: () => new Date() },
  updatedAt: { type: Date, default: () => new Date() },
  createdBy: { type: Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: Schema.Types.ObjectId, ref: 'User' }
}

const Model = defaultProps => {
  return (name, props) => {
    const newSchema = Schema({
      ...defaultProps,
      ...props
    })
    return model(name, newSchema)
  }
}

module.exports = {
  withAudit: Model(auditProps)
}
