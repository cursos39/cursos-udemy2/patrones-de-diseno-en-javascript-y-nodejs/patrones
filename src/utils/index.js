const compose = (...fns) => x => fns.reduceRight((accValue, currentFn) => currentFn(accValue), x)

const head = anyArray => anyArray[0]
const formatPerson = ({ name, surName, age }) => ({
  completeName: `${name} ${surName}`,
  age: age
})
const personToString = ({ completeName, age }) => `${completeName} is ${age} years old`
const filterArray = f => array => array.filter(f)
const isBaby = person => person.age < 2
const trace = label => data => {
  console.log(label, data)
  return data
}

module.exports = {
  compose, head, formatPerson, filterArray, isBaby, personToString, trace
}
