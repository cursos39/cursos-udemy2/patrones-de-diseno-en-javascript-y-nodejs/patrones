const sayHello = name => `Hello I am ${name}`

class Persona {
  constructor () {
    this.name = 'lala'
  }

  saludar () {
    return `Hello I am ${this.name}`
  }
}
class Dog extends Persona {}

class Person {
  constructor () {
    this.name = 'Alberto'
  }

  saludar () {
    return sayHello(this.name)
  }
}

module.exports = {
  Persona, Person, Dog, sayHello
}
