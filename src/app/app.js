const express = require('express')
const axios = require('axios')
const { defaultGet } = require('./handlers')

const app = express()
app.disable('x-powered-by')

app.get('/', defaultGet(axios))

module.exports = app
