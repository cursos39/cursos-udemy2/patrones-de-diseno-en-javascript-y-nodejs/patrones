const Users = (fetchAPI) => {
  const resource = 'https://jsonplaceholder.typicode.com/users'

  return {
    list: () => fetchAPI(resource).then(x => x.json()),
    create: data => fetchAPI(resource, { type: 'POST', body: JSON.stringify(data) }).then(x => x.json())
  }
}

module.exports = { Users }
