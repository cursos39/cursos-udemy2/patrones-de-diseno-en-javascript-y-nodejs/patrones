const { expect } = require('chai')

describe('Recomendations', () => {
  describe('Inmutability', () => {
    it('All variables should be const', () => {
      const example = 'Hello'

      expect(example).to.be.equal('Hello')
    })
    it('Include properties in objects', () => {
      const obj1 = { a: 1 }

      const obj2 = { ...obj1, b: 2 }

      obj1.c = 3

      expect(obj2.b).to.be.equal(2)
      expect(obj1.a).to.be.equal(1)
      expect(obj1.c).to.be.equal(3)
      expect(obj1.b).to.be.equal(undefined)
      expect(obj2.c).to.be.equal(undefined)
    })
  })
})
