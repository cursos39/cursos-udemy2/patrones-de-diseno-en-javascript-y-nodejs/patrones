const { expect } = require('chai')
const { Dog, sayHello, Person } = require('../src/composition-over-inheritance')

describe('Recomendations', () => {
  describe('Composition over inheritance', () => {
    it('Banana Gorila problem', () => {
      expect((new Dog()).saludar()).to.be.equal('Hello I am lala')
    })
    it('Using compsition with objects', () => {
      const persona = {
        name: 'Alberto',
        saludar: () => sayHello(persona.name)
      }

      expect(persona.saludar()).to.be.equal('Hello I am Alberto')
    })
    it('Using compsition with classes', () => {
      expect((new Person()).saludar()).to.be.equal('Hello I am Alberto')
    })
  })
})
