const { expect } = require('chai')
const { withAudit } = require('../../src/db/defaultSchema')

describe('Testing defaultSchema', () => {
  it('Use the schema with audit', () => {
    const productModel = withAudit('Product', { name: String, desc: String })

    expect(productModel().createdAt).to.not.be.equal(undefined)
  })
})
