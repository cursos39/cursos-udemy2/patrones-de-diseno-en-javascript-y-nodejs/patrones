const { expect } = require('chai')
const { Users } = require('../../src/users/index.js')

describe('Testing UsersAPI', () => {
  it('Given mock fetchAPI, when list users sohuld return the fake result', async () => {
    const mockFetch = () => Promise.resolve({ json: () => ({ data: ['value1'] }) })

    const result = await Users(mockFetch).list()

    expect(result.data).to.be.deep.equal(['value1'])
  })

  it('Given mock fetchAPI, when list users sohuld return the fake result', async () => {
    const mockFetch = () => Promise.resolve({ json: () => ({ data: ['value1'] }) })

    const result = await Users(mockFetch).create({ user1: { name: 'Juan' } })

    expect(result.data).to.be.deep.equal(['value1'])
  })
})
