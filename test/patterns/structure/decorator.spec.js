const { expect } = require('chai')
const { HP, mackbook, increaseMemory } = require('./../../../src/patterns/structure/decorator')

describe('Structure patterns. Decorator', () => {
  it('Given Mackbook, when increaseMemory, the price raises', () => {
    expect(mackbook.price).to.be.equal(1000)

    mackbook.increaseMemory()

    expect(mackbook.price).to.be.equal(1100)
  })
  it('Decorator with function', () => {
    const computerHP = new HP()

    increaseMemory(computerHP)

    expect(computerHP.price()).to.be.equal(600)
  })
})
