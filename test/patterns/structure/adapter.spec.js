const { expect } = require('chai')
const { Api, Api2, ApiAdapter } = require('../../../src/patterns/structure/adapter')

describe('Structure patterns. Adapter pattern', () => {
  it('Use of Api', () => {
    const api = new Api()

    expect(api.operations('www.google.cl', { q: 1 }, 'get')).to.be.equal('api1-get')
    expect(api.operations('www.google.cl', { q: 1 }, 'post')).to.be.equal('api1-post')
    expect(api.operations('www.google.cl', { q: 1 }, 'head')).to.be.equal(0)
  })
  it('Use of Api2', () => {
    const api = new Api2()

    expect(api.get('www.google.cl', { q: 1 })).to.be.equal('api2-get')
    expect(api.post('www.google.cl', { q: 1 })).to.be.equal('api2-post')
  })
  it('Use of ApiAdapter', () => {
    const api = new ApiAdapter()

    expect(api.operations('www.google.cl', { q: 1 }, 'get')).to.be.equal('api2-get')
    expect(api.operations('www.google.cl', { q: 1 }, 'post')).to.be.equal('api2-post')
    expect(api.operations('www.google.cl', { q: 1 }, 'head')).to.be.equal(0)
  })
})
