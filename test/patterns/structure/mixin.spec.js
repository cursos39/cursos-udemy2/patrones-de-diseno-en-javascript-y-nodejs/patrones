const { expect } = require('chai')
const { User } = require('./../../../src/patterns/structure/mixin')

describe('Structure patterns', () => {
  describe('Mixin pattern', () => {
    it('Using the mixin pattern in', () => {
      const newUser = new User('Chanchito feliz')

      expect(newUser.sayHello()).to.be.equal(undefined)
      expect(newUser.sayGoodBye()).to.be.equal(undefined)
    })
  })
})
