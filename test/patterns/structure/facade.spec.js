const { expect } = require('chai')
const { restart } = require('../../../src/patterns/creation/object')
const { get, getPromisify } = require('../../../src/patterns/structure/facade')

describe('Structure patterns. Facade', () => {
  before(restart)
  it('Calling get users', (done) => {
    return get('https://jsonplaceholder.typicode.com/users', (error, users) => {
      expect(users).to.be.an('Array')
      done(error)
    })
  })
  it('Get the users with a promise and a facade', () => {
    return getPromisify('https://jsonplaceholder.typicode.com/users').then(users => expect(users).to.be.an('Array'))
  })
})
