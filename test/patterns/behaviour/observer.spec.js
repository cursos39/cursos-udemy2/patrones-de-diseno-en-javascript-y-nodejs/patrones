const { expect } = require('chai')
const { createUser, app, login } = require('./../../../src/patterns/behaviour/observer.js')

describe('Behaviour patterns. Observer pattern', () => {
  it('Given app init and login, user should observe the change', () => {
    const user = createUser()

    app.init(user)

    login(user)

    expect(user.login).to.be.equal(true)
  })
})
