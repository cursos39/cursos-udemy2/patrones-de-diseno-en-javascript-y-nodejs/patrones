const { expect } = require('chai')
const { commander } = require('../../../src/patterns/behaviour/command')

describe('Behaviour patterns', () => {
  describe('Command pattern', () => {
    it('Use the commander to buy a book', () => {
      expect(commander.run('buy', { kind: 'book', title: 'Clean Coder' })).to.be.equal('Buying a book')
    })
    it('Use the commander to sell a chair', () => {
      expect(commander.run('sell', { kind: 'chair', price: 12.00 })).to.be.equal('Selling a chair')
    })
    it('When command not found, return error', () => {
      expect(commander.run('not_a_command', { kind: 'chair', price: 12.00 })).to.be.equal('Command not_a_command not found')
    })
  })
})
