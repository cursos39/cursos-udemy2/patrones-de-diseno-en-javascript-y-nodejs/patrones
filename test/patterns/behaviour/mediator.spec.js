const { expect } = require('chai')
const { Emitter } = require('../../../src/patterns/behaviour/mediator')

describe('Behaviour patterns', () => {
  describe('Mediator pattern', () => {
    it('Should subscribe and emit event', () => {
      Emitter.on('lala', x => {
        x.processed = true
      })

      const variable = { key: 'value' }
      Emitter.emit('lala', variable)

      expect(variable.processed).to.be.equal(true)
    })
  })
})
