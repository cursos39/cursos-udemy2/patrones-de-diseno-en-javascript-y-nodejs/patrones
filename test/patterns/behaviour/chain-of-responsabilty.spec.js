const { expect } = require('chai')
const { Suma } = require('../../../src/patterns/behaviour/chain-of-responsabilty')

describe('Behaviour patterns', () => {
  describe('Chain of responsability pattern', () => {
    it('Sum with responsability pattern', () => {
      const sumador = new Suma(12)

      expect(sumador.val).to.be.equal(12)
      expect(sumador.suma(12).suma(12).val).to.be.equal(36)
    })
  })
})
