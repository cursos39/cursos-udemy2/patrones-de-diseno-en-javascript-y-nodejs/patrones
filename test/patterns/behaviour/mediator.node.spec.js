const { expect } = require('chai')
const Emitter = require('events')

describe('Behaviour patterns', () => {
  describe('Mediator pattern in Node', () => {
    const emitter = new Emitter()
    it('Should subscribe and emit event', () => {
      emitter.on('lala', x => {
        x.processed = true
      })

      const variable = { key: 'value' }
      emitter.emit('lala', variable)

      expect(variable.processed).to.be.equal(true)
    })
  })
})
