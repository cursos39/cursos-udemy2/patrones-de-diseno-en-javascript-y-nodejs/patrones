const { expect } = require('chai')
const { iterator } = require('../../../src/patterns/behaviour/iterator')

describe('Behavioud patterns', () => {
  describe('Iterator pattern', () => {
    it('Given a num array, when iterate over it, should return the elements one at the time', () => {
      const numbers = [1, 2, 3, 4]

      const gen = iterator(numbers)

      expect(gen.next().value).to.be.equal(1)
      expect(gen.next().value).to.be.equal(2)
      expect(gen.next().value).to.be.equal(3)
      expect(gen.next().value).to.be.equal(4)
      expect(gen.next()).to.be.deep.equal({ value: undefined, done: true })
    })
  })
})
