const { kiltro } = require('../../../src/patterns/creation/prototype.js')

describe('Creating object from prototype', () => {
  it('When call ladrar, should console.log', () => {
    kiltro.barf()

    kiltro.race = 'Super dog'

    kiltro.barf()
  })
})
