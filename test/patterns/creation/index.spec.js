const { expect } = require('chai')
const { MyClass, MyFunctionClass, MyClassWithProto } = require('../../../src/patterns/creation/index')

describe('Creational patterns', () => {
  describe('Creating objects with classes', () => {
    it('Should create a new instance with new', () => {
      const instance = new MyClass()

      expect(instance).to.not.be.equal(undefined)
    })
    it('Should return metodo', () => {
      const instance = new MyClass()

      expect(instance.metodo()).to.be.equal(1)
    })
  })
  describe('Create objects with factory method and new keyword', () => {
    it('Should cfreate new element from function', () => {
      const instance = new MyFunctionClass()

      expect(instance).to.not.be.equal(undefined)
    })
    it('Should return met', () => {
      const instance = new MyFunctionClass()

      expect(instance.met()).to.be.equal(1)
    })
  })
  describe('Create objects with class with prototype and new keyword', () => {
    it('Should cfreate new element from function', () => {
      const instance = new MyClassWithProto(1, 2)

      expect(instance).to.not.be.equal(undefined)
    })
    it('Should return metodo', () => {
      const instance = new MyClassWithProto(1, 2)

      expect(instance.metodo()).to.be.equal(1)
    })
  })
})
