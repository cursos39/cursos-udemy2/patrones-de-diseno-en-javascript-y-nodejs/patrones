const { expect } = require('chai')
const { modulo, moduleRevelated } = require('../../../src/patterns/creation/modulo.js')

describe('Module pattern', () => {
  it('Should say cache is enabled', () => {
    expect(modulo.isCacheEnabled()).to.be.equal('yes')
  })
  it('Given config changed, isCacheEnabled shuold return no', () => {
    const previousConfig = modulo.config

    modulo.setConfig({ cache: false })

    expect(modulo.isCacheEnabled()).to.be.equal('no')

    modulo.setConfig(previousConfig)
  })
})

describe('Module revelated', () => {
  it('Should operate with module revelated', () => {
    const myObject = moduleRevelated

    console.log(myObject.returnValue())

    expect(myObject.returnValue()).to.be.deep.equal({})
  })
  it('Given set property, when getValue should return new element', () => {
    const myObject = moduleRevelated

    myObject.setProperty('cheese', 'goat')

    expect(myObject.returnValue()).to.be.deep.equal({ cheese: 'goat' })
  })
})
