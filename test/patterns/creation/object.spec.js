const { expect } = require('chai')
const { restart, modifyPrototypes } = require('../../../src/patterns/creation/object.js')

describe('Creation patterns. Object pattern', () => {
  before(modifyPrototypes)
  describe('Create object', () => {
    it('Use the log inside objects', () => {
      const x = { a: 1 }

      expect(x).to.have.property('log')
      expect(x.log()).to.be.equal(undefined)
    })
  })

  describe('String myTrim in prototype', () => {
    [
      { input: '  asdfg  ', expected: 'asdfg' },
      { input: '', expected: '' }
    ]
      .forEach(element => it(`Should trim the string ${element.input}`, () => {
        expect(element.input.myTrim()).to.be.equal(element.expected)
      }))
  })

  after(() => restart())
})
