const { expect } = require('chai')
const { f } = require('./../../../src/patterns/functional/point-free.js')

describe('Point Free: using callbacks', () => {
  it('When call f, the result should be in the callback. Manage with point free', done => {
    const handleResult = result => {
      expect(result.ok).to.be.equal(true)
      done()
    }
    f('users', handleResult)
  })
})
