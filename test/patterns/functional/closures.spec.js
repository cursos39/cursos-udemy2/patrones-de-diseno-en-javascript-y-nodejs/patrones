const { expect } = require('chai')
const { closure, closureWithParam } = require('./../../../src/patterns/functional/closure.js')

describe('Show closures scope', () => {
  it('When call closure returns all the variables in the scope and above', () => {
    const { x, y, z } = closure()

    console.log(x, y, z)

    expect(x).to.not.be.equal(undefined)
  })
  it('Given closure with params, when call it, we can get new param modified', () => {
    const result = closureWithParam('Alberto')()

    expect(result).to.be.equal('Damm it, Alberto')
  })
})
