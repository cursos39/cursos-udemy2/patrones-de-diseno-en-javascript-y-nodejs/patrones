const { expect } = require('chai')
const { sum, curriedSum } = require('./../../../src/patterns/functional/curry')

describe('Curried vs normal', () => {
  it('Given two numbers, when sum, should return sum', () => {
    expect(sum(1, 5)).to.be.equal(6)
  })
  it('Given sum1, when call with 5 should return 6 (curried)', () => {
    const sum1 = curriedSum(1)

    expect(sum1(5)).to.be.equal(6)
  })
})
