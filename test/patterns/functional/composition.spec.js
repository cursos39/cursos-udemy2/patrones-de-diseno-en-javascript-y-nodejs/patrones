const { expect } = require('chai')
const { bringFirstBaby, users } = require('../../../src/patterns/functional/manage-users')

describe('Composition examples', () => {
  describe('Examples I', () => {
    it('Should get firstBaby of the users', () => {
      expect(bringFirstBaby(users)).to.be.equal('Sofia Zapata is 1 years old')
    })
  })
})
