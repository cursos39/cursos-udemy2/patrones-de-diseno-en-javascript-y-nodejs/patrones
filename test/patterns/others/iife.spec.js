const { expect } = require('chai')
const { getIIFE } = require('../../../src/patterns/others/iife')

describe('Other patterns', () => {
  describe('IIFE pattern', () => {
    const resultado = getIIFE()

    expect(resultado).to.be.equal(12)
  })
})
