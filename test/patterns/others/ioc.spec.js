const { expect } = require('chai')
const { restart } = require('../../../src/patterns/creation/object')
const request = require('supertest')
const app = require('../../../src/app/app.js')

describe('Other patterns', () => {
  before(restart)
  describe('Inversion of Control pattern', () => {
    it('Should get the / route in the app', () => {
      return request(app).get('/').expect(200).then(response => {
        expect(response.body).to.be.an('array')
      })
    })
  })
})
