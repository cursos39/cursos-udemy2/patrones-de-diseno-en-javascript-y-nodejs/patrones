describe('Recomendations', () => {
  describe('Async Await', () => {
    const ajaxCb = cb => cb()
    const ajax = () => Promise.resolve()
    it('Async Await examples. Callback hell', () => {
      const fn = () => {
        ajaxCb(() => {
          ajaxCb(() => {
            ajaxCb(() => {})
          })
        })
      }
    })
    it('Async Await examples. Promises problems', () => {
      const fn1 = () => {
        ajax()
          .then(() => {
            const a = 1
            return ajax()
          })
          .then(() => {
            const a = 1
            return ajax()
          })
          .then(() => {
            const a = 1
            return ajax()
          })
          .then(() => {
            console.log(a)
            return ajax()
          })
      }
    })
    it('Resolving with async/await', () => {
      const fn = async () => {
        const result1 = await ajax()
        const result2 = await ajax()
        const result3 = await ajax()
      }
    })
  })
})
