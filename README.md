[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=patrones&metric=alert_status)](https://sonarcloud.io/dashboard?id=patrones)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=patrones&metric=coverage)](https://sonarcloud.io/dashboard?id=patrones)


# Sección de patrones


Tipos:

1. Patrones creacionales
1. Patrones estructurales
1. Patrones de comportamiento

## Section 2: Patrones creacionales

6. Intro.
7. Patrón constructor.
8. Patrón constructor con prototipo.
9. Ejemplo práctico de patrón constructor, con prototipo.
10. Patrón módulo.
11. Patrón módulo revelador.
12. Ejemplo patrón módulo revelador.
13. Patrón prototipo.

## Section 3: Patrones funcionales

14. Closures I.
15. Closures II.
16. Programación tácita o point free.
17. Currying.
18. Composición I.
19. Composición II.
 ```js
 f(g(x))
 // Equivalente a
 const a = g(x)
 const r = f(a)
 return r
 ```
20. Composición III.
21. Composición IV.

## Section 4: Patrones estructurales

22. Mixin.
23. Decorator.
24. Facade.
25. Adapter.

## Section 5: Patrones de comportamiento

26. Patrón observador.
27. Patrón mediador.
28. Mediador en NodeJS
29. Comando.
30. Cadena de responsabilidad.
31. Iterador.

## Section 6: Otros patrones

32. Patrón IIFE.
33. Inyección de dependencias (Inversión de Control).

## Section 7: Recomendaciones

34. Intro
35. Composición sobre herencia.
36. Inmutabilidad.
37. async/await

# Section 8: Uso Práctico

38. Introducción.
39. Usando patrones I.
40. Usando patrones II.
41. Clase extra.

